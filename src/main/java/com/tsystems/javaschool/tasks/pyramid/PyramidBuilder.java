package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        long hh, s = 0;
        for (hh = (long) Math.sqrt(inputNumbers.size()); s < inputNumbers.size(); hh++) {
            s = (1 + hh) * hh / 2;
        }
        if (inputNumbers.size() != s || inputNumbers.stream().anyMatch(Objects::isNull)) {
            throw new CannotBuildPyramidException();
        }
        int height = (int) (hh - 1);
        int[][] res = new int[height][2 * height - 1];
        Collections.sort(inputNumbers);
        int a = 0;
        for (int i = 0, j = height - 1; i < height; i++, j--) {
            int b = j;
            for (int k = 0; k < i + 1; k++) {
                res[i][b] = inputNumbers.get(a);
                a++;
                b += 2;
            }
        }
        return res;
    }
}
