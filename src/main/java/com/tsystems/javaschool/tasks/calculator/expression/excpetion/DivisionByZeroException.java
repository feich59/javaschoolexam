package com.tsystems.javaschool.tasks.calculator.expression.excpetion;

public class DivisionByZeroException extends Exception {
    public DivisionByZeroException(String message) {
        super(message);
    }
}
