package com.tsystems.javaschool.tasks.calculator.expression;

import com.tsystems.javaschool.tasks.calculator.expression.excpetion.DivisionByZeroException;

public interface Expression {
    double calc() throws DivisionByZeroException;
}