package com.tsystems.javaschool.tasks.calculator.expression;

public class Add extends BinaryOperator implements Expression {

    public Add(Expression a, Expression b) {
        super(a, b);
    }

    @Override
    protected double doCalc(double a, double b) {
        return a + b;
    }
}