package com.tsystems.javaschool.tasks.calculator.expression;

import com.tsystems.javaschool.tasks.calculator.expression.excpetion.DivisionByZeroException;

public abstract class BinaryOperator implements Expression {

    private final Expression a;
    private final Expression b;

    protected BinaryOperator(Expression a, Expression b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calc() throws DivisionByZeroException {
        double l = a.calc();
        double r = b.calc();
        return doCalc(l, r);
    }

    protected abstract double doCalc(double a, double b) throws DivisionByZeroException;
}
