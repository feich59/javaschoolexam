package com.tsystems.javaschool.tasks.calculator.parser.tokenizer.exception;

public class TokenException extends Exception {
    public TokenException(String s) {
        super(s);
    }
}
