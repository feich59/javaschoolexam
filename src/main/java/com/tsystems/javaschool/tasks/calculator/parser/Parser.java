package com.tsystems.javaschool.tasks.calculator.parser;

import com.tsystems.javaschool.tasks.calculator.expression.*;
import com.tsystems.javaschool.tasks.calculator.parser.exception.MyParseException;
import com.tsystems.javaschool.tasks.calculator.parser.tokenizer.Tokenizer;
import com.tsystems.javaschool.tasks.calculator.parser.tokenizer.Tokens;
import com.tsystems.javaschool.tasks.calculator.parser.tokenizer.exception.TokenException;

public class Parser {

    private Tokenizer tokenizer;

    public Expression parse(String s) throws TokenException, MyParseException {
        if (s == null) {
            throw new IllegalArgumentException();
        }
        tokenizer = new Tokenizer(s);
        tokenizer.next();
        return parseExpression();
    }

    private Expression parseNum() throws TokenException, MyParseException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case NUM:
                expr = new Const(tokenizer.getNum());
                tokenizer.next();
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing number ", tokenizer.getToken().toString()));
        }
        return expr;
    }

    private Expression parseContMulDiv(Expression l) throws MyParseException, TokenException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case MUL:
                tokenizer.next();
                expr = parseContMulDiv(new Mul(l, parseAtom()));
                break;
            case DIV:
                tokenizer.next();
                expr = parseContMulDiv(new Div(l, parseAtom()));
                break;
            case CLOSE:
            case ADD:
            case SUB:
            case EOI:
                expr = l;
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing contmuldiv ", tokenizer.getToken().toString()));
        }
        return expr;
    }

    private Expression parseAtom() throws TokenException, MyParseException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case NUM:
                expr = parseNum();
                break;
            case OPEN:
                tokenizer.next();
                expr = parseExpression();
                if (tokenizer.getToken() != Tokens.CLOSE)
                    throw new MyParseException("Expected close");
                tokenizer.next();
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing atom", tokenizer.getToken().toString()));
        }
        return expr;
    }

    private Expression parseContAddSub(Expression l) throws TokenException, MyParseException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case ADD:
                tokenizer.next();
                expr = parseContAddSub(new Add(l, parseContMulDiv(parseAtom())));
                break;
            case SUB:
                tokenizer.next();
                expr = parseContAddSub(new Sub(l, parseContMulDiv(parseAtom())));
                break;
            case CLOSE:
            case EOI:
                expr = l;
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing contaddsub ", tokenizer.getToken().toString()));
        }
        return expr;
    }

    private Expression parseAddSub() throws MyParseException, TokenException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case NUM:
            case OPEN:
                expr = parseContMulDiv(parseAtom());
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing addsub ", tokenizer.getToken().toString()));
        }
        return expr;
    }

    private Expression parseExpression() throws TokenException, MyParseException {
        Expression expr;
        switch (tokenizer.getToken()) {
            case NUM:
            case OPEN:
                expr = parseContAddSub(parseAddSub());
                break;
            default:
                throw new MyParseException(String.format("Unexpected token %s while parsing expression ", tokenizer.getToken().toString()));
        }

        return expr;
    }
}
