package com.tsystems.javaschool.tasks.calculator.parser.exception;

public class MyParseException extends Exception {
    public MyParseException(String s) {
        super(s);
    }

    public MyParseException(String s, Exception cause) {
        super(s, cause);
    }
}
