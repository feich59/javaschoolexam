package com.tsystems.javaschool.tasks.calculator.parser.tokenizer;

public enum Tokens {
    NUM,
    ADD,
    SUB,
    MUL,
    DIV,
    OPEN,
    CLOSE,
    EOI
}
