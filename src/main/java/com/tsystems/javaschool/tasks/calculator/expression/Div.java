package com.tsystems.javaschool.tasks.calculator.expression;

import com.tsystems.javaschool.tasks.calculator.expression.excpetion.DivisionByZeroException;

public class Div extends BinaryOperator implements Expression {

    public Div(Expression a, Expression b) {
        super(a, b);
    }

    @Override
    protected double doCalc(double a, double b) throws DivisionByZeroException {
        if (b == 0) {
            throw new DivisionByZeroException("Division by zero is not allowed");
        }
        return a / b;
    }
}
