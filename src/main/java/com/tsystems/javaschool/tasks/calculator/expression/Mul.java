package com.tsystems.javaschool.tasks.calculator.expression;

public class Mul extends BinaryOperator implements Expression {

    public Mul(Expression a, Expression b) {
        super(a, b);
    }

    @Override
    protected double doCalc(double a, double b) {
        return a * b;
    }
}