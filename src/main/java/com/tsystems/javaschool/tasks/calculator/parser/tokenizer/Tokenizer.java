package com.tsystems.javaschool.tasks.calculator.parser.tokenizer;

import com.tsystems.javaschool.tasks.calculator.parser.exception.MyParseException;
import com.tsystems.javaschool.tasks.calculator.parser.tokenizer.exception.TokenException;

public class Tokenizer {

    private String s;
    private int i = 0;
    private double num;
    private Tokens token;

    public Tokenizer(String s) {
        this.s = s;
    }

    public void next() throws TokenException, MyParseException {
        if (!skipSpace()) {
            token = Tokens.EOI;
            return;
        }

        if (isDigit(s.charAt(i))) {
            parseDigit();
            token = Tokens.NUM;
            return;
        }

        switch (s.charAt(i)) {
            case '+':
                i++;
                token = Tokens.ADD;
                return;
            case '-':
                i++;
                token = Tokens.SUB;
                return;
            case '/':
                i++;
                token = Tokens.DIV;
                return;
            case '*':
                i++;
                token = Tokens.MUL;
                return;
            case '(':
                i++;
                token = Tokens.OPEN;
                return;
            case ')':
                i++;
                token = Tokens.CLOSE;
                return;
        }
        throw new TokenException(String.format("Unknown character %c at position %d", s.charAt(i), i + 1));
    }

    public double getNum() {
        return num;
    }

    public Tokens getToken() {
        return token;
    }

    private boolean skipSpace() {
        while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
            i++;
        }
        return i < s.length();
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private void parseDigit() throws MyParseException {
        StringBuilder sb = new StringBuilder();
        while (i < s.length() && (isDigit(s.charAt(i)) || s.charAt(i) == '.')) {
            sb.append(s.charAt(i++));
        }
        try {
            num = Double.parseDouble(sb.toString());
        } catch (NumberFormatException e) {
            throw new MyParseException("Wrong number format", e);
        }
    }

}
