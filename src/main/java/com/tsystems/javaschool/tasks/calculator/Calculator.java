package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.expression.excpetion.DivisionByZeroException;
import com.tsystems.javaschool.tasks.calculator.parser.Parser;
import com.tsystems.javaschool.tasks.calculator.parser.exception.MyParseException;
import com.tsystems.javaschool.tasks.calculator.parser.tokenizer.exception.TokenException;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Math.round;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Parser parser = new Parser();
        try {
            double ans = parser.parse(statement).calc();
            if (round(ans) == ans) {
                return String.valueOf((int) ans);
            } else {
                return String.valueOf(new BigDecimal(ans).setScale(4, RoundingMode.HALF_UP).stripTrailingZeros());
            }
        } catch (TokenException | MyParseException | DivisionByZeroException | IllegalArgumentException e) {
            return null;
        }
    }
}
