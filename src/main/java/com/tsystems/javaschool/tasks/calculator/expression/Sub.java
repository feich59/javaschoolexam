package com.tsystems.javaschool.tasks.calculator.expression;

public class Sub extends BinaryOperator implements Expression {

    public Sub(Expression a, Expression b) {
        super(a, b);
    }

    @Override
    protected double doCalc(double a, double b) {
        return a - b;
    }
}