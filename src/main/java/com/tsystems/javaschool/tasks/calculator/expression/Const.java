package com.tsystems.javaschool.tasks.calculator.expression;

public class Const implements Expression {

    private final double numb;

    public Const(double a) {
        numb = a;
    }

    @Override
    public double calc() {
        return numb;
    }
}