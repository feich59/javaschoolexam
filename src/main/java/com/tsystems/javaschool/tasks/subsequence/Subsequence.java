package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        int n = x.size();
        int m = y.size();
        boolean[][] d = new boolean[n + 1][m + 1];
        for (int j = 0; j < m + 1; j++) {
            d[0][j] = true;
        }

        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < m + 1; j++) {
                d[i][j] = d[i][j - 1] || (d[i - 1][j - 1] && y.get(j - 1) == x.get(i - 1));
            }
        }
        return d[n][m];
    }
}
